<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wptreehouse_photography_theme');

/** MySQL database username */
define('DB_USER', 'wptreehousecom');

/** MySQL database password */
define('DB_PASSWORD', 'wpHqRysD');

/** MySQL hostname */
define('DB_HOST', 'mysql.wptreehouse.com');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_HOME', 'http://wptreehouse.com/photography-theme'); 
define('WP_SITEURL', 'http://wptreehouse.com/photography-theme'); 

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '*>%27L>BU|h($-#`huIygM-^-%,&vMvON7SGS/g7l<%C>s*t_;>~;p$7LEY7t0bE');
define('SECURE_AUTH_KEY',  'oiLC>.Ehy@[_*`|~v{Y``XMYuv-krFe]vUG)8-9FO`4uMG_:x:BR,lodVYP}&~Q;');
define('LOGGED_IN_KEY',    'd>aWfpv/];J-%c t,OP*jy1YQ MJwCK20[9_s+TSRQk2m-K,;~Mm=)nr+*}!Pk>+');
define('NONCE_KEY',        ' Bg5C)+`wk9=l`l,KwYTw=cZK8MXz;xL5p %To+9^JR8y71k@|NS?=h+i%tyVME5');
define('AUTH_SALT',        'dkKr2CG:Y~j0|ye]Z%h=iGigBh3n%qyQ*^-o|ngjE3J+upF+GjGpUru{@0M$7def');
define('SECURE_AUTH_SALT', 'MVv@Q;PKA8<TrQ9ok~Oi|CG3</<{Mwu%Xj&aU>*}i}Z_Gc*pQh)k-;TZgxFn)0|x');
define('LOGGED_IN_SALT',   'hQbWB{QLCdlD2=u<=@{Qm./$ XIi;~nt:u7TLlOV[TqZ7y5<30o PMzD}-EvF4+,');
define('NONCE_SALT',       'IWiG^2|v+Y|4oUADRqIZ<PEsF:I-V%^0x2,0hly.zd8&V0H{5cN!oWHp39CD7#<K');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wppt_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
