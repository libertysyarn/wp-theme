jQuery(document).ready(function($){

	$('.treehouse-badge').hover(function() {
		$(this).find('.treehouse-badge-info').stop(true, true).fadeIn(200);
	}, function() {
		$(this).find('.treehouse-badge-info').stop(true, true).fadeOut(200);
	});
});