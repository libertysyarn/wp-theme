<?php 
/*
	Plugin Name: Treehouse Badges Plugin
	Plugin URI: http://teamtreehouse.com
	Description: A plugin that pulls in your badges from your Treehouse profile account
	Version: 1.0
	Author: Zac Gordon
	Author URI: http://zacgordon.com
	License: GPL2
*/

/** Step 2 (from text above). */
add_action( 'admin_menu', 'treehouse_profile_badges_menu' );

/** Step 1. */
function treehouse_profile_badges_menu() {
	add_options_page( 'Treehouse Badges', 'Treehouse Badges', 'manage_options', 'treehouse-badges', 'treehouse_profile_badges_options' );
}

/** Step 3. */

function treehouse_profile_badges_admin_css() {
   echo '<style type="text/css">
           img.gravatar {max-width: 100%;}
           .inside {display: table}
           .badges > li {float: left; margin: 5px; text-align: center; min-height: 250px; width: 120px;}           
           .badges li li {margin: 2px 0; min-height: auto}
           .badges img {float: left; width: 100%; margin-bottom: 5px;}           
           .project-name a {font-size: .8em; text-decoration: none; line-height: .5em}
           .badges-and-points li {padding: 5px 10px; background: #474747; color: #ddd; border-radius: 20px; font-size: 1.2em; float: left;}    
           .badges-and-points li:first-child {margin-right: 5px}       
           .badges-and-points strong {color: #92ae57;}           
           .referral-code {width: 100%; height: 80px}
         </style>';
}
add_action('admin_head', 'treehouse_profile_badges_admin_css');
	

function treehouse_profile_badges_front_end() {    
    wp_enqueue_style( 'front-end-css', plugins_url('css/front-end.css', __FILE__) );    
    wp_enqueue_script( 'front-end-js', plugins_url('/js/front-end.js', __FILE__), array('jquery'), '', true );
}
add_action( 'wp_enqueue_scripts', 'treehouse_profile_badges_front_end' );



function treehouse_profile_badges_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}    

	// Display JSON Feed
	// Optional setting to display JSON feed in admin area
	$display_json = false;

    $hidden_field_name = esc_html($_POST[ 'form_submitted' ]);	

	// If settings have been updated
    if( isset( $hidden_field_name ) && $hidden_field_name == 'Y' ) {           

        // Get submitted username
        $username = esc_html($_POST[ 'treehouse_username' ]);               

        // If username is set update options 
        //if( $username != '' ) {
			
	        update_option( 'treehouse_username', $username );    	     	        

	        // Get and update the user profile
	        $user_json_feed = 'http://teamtreehouse.com/' . $username . '.json';        
	        $profile = get_treehouse_profile($user_json_feed);

	        update_option( 'treehouse_profile', $profile );	        
        //}

        echo '<div class="updated"><p><strong>';
        _e("Settings saved.", "treehouse_badges");
        echo '</strong></p></div>';

    }

	$profile = get_option( 'treehouse_profile' );	
	$username = get_option( 'treehouse_username' );
	$profile_url = "http://teamtreehouse.com/$username";
	$total_treehouse_badges = count($profile->{'badges'});		


	/*

		Begin Admin Setting Page
	
	==============================================
		
	*/

	?>

	<div class="wrap">
		
		<div id="icon-options-general" class="icon32"></div>
		<h2> <?php _e( 'Official Treehouse Badges Plugin' ) ?> </h2>
		
		<div id="poststuff">
		
			<div id="post-body" class="metabox-holder columns-2">		 

				<!-- main content -->
				<div id="post-body-content">
					

				<?php if( $username == '' || $profile == '' ): ?>
					<?php if($username == ''): ?>
					<div class="error">
						<p><strong><?php _e("Please enter in your username.", "treehouse_badges"); ?></strong></p>
					</div>
					<?php endif; ?>
					
					<?php if($username != '' && $profile == ''): ?>
					<div class="error">
						<p><strong><?php _e("Uh oh! We could not find that username, please try again.", "treehouse_badges"); ?></strong></p>
					</div>
					<?php endif; ?>
					
					<div class="meta-box-sortables ui-sortable">
						
						<div class="postbox">
						
							<h3><span>Let's Get Started!</span></h3>
							<div class="inside">								
								
								<form name="form1" method="post" action="">
								<input type="hidden" name="form_submitted" value="Y">

								<table class="form-table">
								<tbody>
									<tr valign="top">
										<th scope="row">
											<label for="treehouse_username"><?php _e("Treehouse username:", 'treehouse_username' ); ?></label>
										</th>
										<td>
											<input type="text" name="treehouse_username" value="<?php echo $username; ?>">
										</td>
									</tr>
								</tbody>
								</table>

									<p>
										<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save') ?>" />										
									</p>

								</form>

							</div> <!-- .inside -->
						
						</div> <!-- .postbox -->
						
					</div> <!-- .meta-box-sortables .ui-sortable -->

				<?php else: ?>		

					<div class="meta-box-sortables ui-sortable">
						
						<div class="postbox">
						
							<h3><span>Most Recent Badges</span></h3>							
							<div class="inside">
								<p>Below are your most 12 most recent badges.  You can change how many badges you want to display on the front end of the site using the individual widgets settings under Appearance &gt; Widgets.</p>
								<ul class="badges">
								<?php
									for($i = $total_treehouse_badges - 1; $i >= $total_treehouse_badges - 12; $i--):
									$course_url = $profile->{'badges'}[$i]->{'url'};
								?>
									<li>
										<ul>
											<li><img class="badge" title="<?php echo $profile->{'badges'}[$i]->{'name'}; ?>" src="<?php echo $profile->{'badges'}[$i]->{'icon_url'}; ?>">
										
											<li class="badge-name">
												<?php if($profile->{'badges'}[$i]->{'url'} != $profile_url): ?>
												<a target="_blank" href="<?php echo $profile->{'badges'}[$i]->{'url'}; ?>">
													<?php echo $profile->{'badges'}[$i]->{'name'}; ?>
												</a>
												<?php else: ?>
													<?php echo $profile->{'badges'}[$i]->{'name'}; ?>	
												<?php endif; ?>
											</li>

											<?php if ( $profile->{'badges'}[$i]->{'courses'}[1]->{'title'} ): ?>
											<li class="project-name">
												<a target="_blank" href="<?php echo $profile->{'badges'}[$i]->{'courses'}[1]->{'url'}; ?>">
												<?php echo $profile->{'badges'}[$i]->{'courses'}[1]->{'title'}; ?>
												</a>
											</li>
											<?php endif; ?>
										</ul>
									</li>
								<?php endfor; ?>								
								</ul>								
													
							</div> <!-- .inside -->
						
						</div> <!-- .postbox -->
						
					</div> <!-- .meta-box-sortables .ui-sortable -->

					<?php if( $display_json == true ): ?>
					<div class="meta-box-sortables ui-sortable">
						
						<div class="postbox">
						
							<h3><span>JSON Feed</span></h3>							
							<div class="inside">
									<pre><code><?php echo var_dump($profile); ?></pre></code>
							</div> <!-- .inside -->
						
						</div> <!-- .postbox -->
						
					</div> <!-- .meta-box-sortables .ui-sortable -->
					<?php endif; ?>
					
				</div> <!-- post-body-content -->
				
				<!-- sidebar -->
				<div id="postbox-container-1" class="postbox-container">					
						

					<div class="meta-box-sortables">

						<div class="meta-box-sortables ui-sortable">
							
							<div class="postbox">
							
								<h3><span><?php echo $profile->{'name'} ;?>'s Profile</span></h3>
								<div class="inside">														
								<p><img class="gravatar" src="<?php echo $profile->{'gravatar_url'}; ?>"></p>
								<ul class="badges-and-points">
									<li>Badges: <strong><?php echo $total_treehouse_badges ;?></strong></li>
									<li>Points: <strong><?php echo $profile->{'points'}->total ;?></strong></li>
								</ul>
								<br>
								<br>
								<form name="form2" method="post" action="">
									<input type="hidden" name="form_submitted" value="Y">
									<p>
										<label for="treehouse_username"><?php _e("Treehouse username:", 'treehouse_username' ); ?></label>
									</p>
										<input type="text" name="treehouse_username" value="<?php echo $username; ?>">
								
										<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Update') ?>" />										
									</p>

								</form>
							</div>
		
						</div> <!-- .postbox -->
						
					</div> <!-- .meta-box-sortables -->					


					<div class="meta-box-sortables">

						<div class="meta-box-sortables ui-sortable">
							
							<div class="postbox">
							
								<h3><span>Treehouse Referral Badge</span></h3>
								
								<div class="inside">	
									<p>Refer 5 more members and get your Treehouse membership totally free!</p>
									<p><a href="http://referrals.trhou.se/<?php echo $username; ?>" target="_blank"><img src="http://teamtreehouse.com/referral-badge/<?php echo $username; ?>"/></a></p>
									<p>Just add the code below to a template file, page or widget:</p>
									<textarea class="referral-code">&lt;a href=&quot;http://referrals.trhou.se/<?php echo $username; ?>&quot; target=&quot;_blank&quot;&gt;&lt;img src=&quot;http://teamtreehouse.com/referral-badge/<?php echo $username; ?>&quot;/&gt;&lt;/a&gt;</textarea>
								</div>
							
							</div>
		
						</div> <!-- .postbox -->
						
					</div> <!-- .meta-box-sortables -->

					
				</div> <!-- #postbox-container-1 .postbox-container -->

				<?php endif; ?>
				
			</div> <!-- #post-body .metabox-holder .columns-2 -->
			
			<br class="clear">
		</div> <!-- #poststuff -->
		
	</div> <!-- .wrap -->

<?php

}

function get_treehouse_profile($url = '') {	
	
	if( !class_exists( 'WP_Http' ) ) include_once( ABSPATH . WPINC. '/class-http.php' );      	
	if( $url == '' ) $url = get_option( 'treehouse_json_feed' );			
	
	$request = new WP_Http; 
	$profile = $request->request( $url , array('') );		
	if( isset($profile->{'errors'}) ) $profile = null;

	$profile = json_decode($profile['body']);		
	return $profile;
}



/**
 * Example Widget Class
 */
class treehouse_badge_widget extends WP_Widget {
 
    /** constructor -- name this the same as the class above */
    function treehouse_badge_widget() {
        parent::WP_Widget(false, $name = 'Treehouse Badge Widget');	
    }

    /** @see WP_Widget::widget -- do not rename this */
    function widget($args, $instance) {	
        extract( $args );
        $title 	= apply_filters('widget_title', $instance['title']);        
        $numb_badges_to_display = $instance['numb_badges_to_display'];        
        $display_tool_tip = $instance['display_tool_tip'];

		$profile = get_option( 'treehouse_profile' );
        $total_badges = count($profile->{'badges'});

?>
 
		<?php echo $before_widget; ?>
		  <?php if ( $title )
		        echo $before_title . $title . $after_title; ?>

				<ul class="treehouse-badges">
				<?php for($i = $total_badges - 1; $i >= $total_badges - $numb_badges_to_display; $i--): ?>
					<?php $project_url = $profile->{'badges'}[$i]->{'url'}; ?>
					<li class="treehouse-badge">						
						

						<img title="<?php echo $profile->{'badges'}[$i]->{'name'}; ?>" src="<?php echo $profile->{'badges'}[$i]->{'icon_url'}; ?>">
						

						<?php if ( $display_tool_tip == "1" ): ?>
						<div class="treehouse-badge-info" style="display:none">
																					
							<p class="treehouse-badge-name">			
								<a target="_blank" href="<?php echo $project_url; ?>">
									<?php echo $profile->{'badges'}[$i]->{'name'}; ?>
								</a>								
							</p>							

										
							<?php if ( $profile->{'badges'}[$i]->{'courses'}[1]->{'title'} != '' ): ?>
							
							<p class="treehouse-badge-project">
								<a target="_blank" href="<?php echo $profile->{'badges'}[$i]->{'courses'}[1]->{'url'}; ?>">
								<?php echo $profile->{'badges'}[$i]->{'courses'}[1]->{'title'} ;?>
								</a>
							</p>
							<?php endif; ?>

							<a href="http://teamtreehouse.com" alt="Team Treehouse | A Better Way to Learn Technology" class="treehouse-logo">
								<img src="<?php echo site_url(); ?>/wp-content/plugins/treehouse-profile-badges/images/treehouse-logo-full.png" title="Team Treehouse" />
							</a>
								
							<span class="tooltip bottom"></span>							

						</div>				
						<?php endif; ?>

						<?php 
							// echo '<pre><code>';		
							// echo var_dump($profile->{'badges'}[$i]->{'courses'}[1]->{'title'});
							// echo '</pre></code>';			
						 ?>

					</li>
				<?php endfor; ?>		
				</ul>				
		<?php echo $after_widget; ?>
<?php
    }
 
    /** @see WP_Widget::update -- do not rename this */
    function update($new_instance, $old_instance) {		
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['numb_badges_to_display'] = strip_tags($new_instance['numb_badges_to_display']);		
		$instance['display_tool_tip'] = strip_tags($new_instance['display_tool_tip']);		
        return $instance;
    }
 
    /** @see WP_Widget::form -- do not rename this */
    function form($instance) {	

        $title = esc_attr($instance['title']);
        $numb_badges_to_display = esc_attr($instance['numb_badges_to_display']);   
        $display_tool_tip = esc_attr($instance['display_tool_tip']);   
	
		$profile = get_option( 'treehouse_profile' );
		$total_badges = count($profile->{'badges'});

?>
        <p>
          <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
          <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
        <p>
        	<?php 
        		_e('Total badges:');   
        		echo ' ' . $total_badges;
        	?> 
        </p>
        <p>
          <label for="<?php echo $this->get_field_id('numb_badges_to_display'); ?>"><?php _e('How many of your most recent badges would you you like to display?'); ?></label> 
          <input class="" size="4" id="<?php echo $this->get_field_id('numb_badges_to_display'); ?>" name="<?php echo $this->get_field_name('numb_badges_to_display'); ?>" type="text" value="<?php echo $numb_badges_to_display; ?>" />
        </p>
        <p>
          <label for="<?php echo $this->get_field_id('display_tool_tip'); ?>"><?php _e('Display tooltips?'); ?></label> 
          <input type="checkbox" id="<?php echo $this->get_field_id('display_tool_tip'); ?>" name="<?php echo $this->get_field_name('display_tool_tip'); ?>" value="1" <?php checked( $display_tool_tip, 1 ); ?> />
        </p>
        <?php 
    }
 
 
} // end class example_widget
add_action('widgets_init', create_function('', 'return register_widget("treehouse_badge_widget");'));


?>