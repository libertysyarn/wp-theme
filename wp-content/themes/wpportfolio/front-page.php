<?php get_header(); ?>

</div>
<div id="featured" class="clearfix flexslider">
	
	<ul class="slides">
    <?php 
        $args = array(
            'post_type' => 'work'
        );
        $slideshow_query = new WP_Query( $args );          
    ?>			
	<?php if ( have_posts() ) : while ( $slideshow_query->have_posts() ) : $slideshow_query->the_post(); ?>
		<li style="background-color: <?php the_field('background_color'); ?>">
			<div class="container">
				<div class="grid_8"><img src="<?php the_field('homepage_slider_image'); ?>"></div>
				<div id="featured-info" class="grid_4 omega">
					<h5>Featured Project</h5>
					<h3 style="color: <?php the_field('button_color'); ?>"><?php the_title(); ?></h3>
					<p><?php the_field('description'); ?></p>
					<p><a class="btn blue" style="background-color: <?php the_field('button_color'); ?>" href="<?php the_permalink(); ?>">View Project &rarr;</a></p>
				</div>
			</div>				
		
		</li>

	<?php endwhile; else: ?>
		
		<p>There are no posts or pages here</p>

	<?php endif; ?>
	</ul>

</div>
<div class="container clearfix">

<div class="grid_12 omega">
	<h5>Featured Post</h5>
</div>

<div class="push_2 grid_10 omega clearfix">
	<article>			
	    <?php 
		    wp_reset_postdata();
	        $args = array(
	            'post_type' => 'post',
	            'category_name' => 'featured',
	            'posts_per_page' => 1
	        );
	        $the_query = new WP_Query( $args );		    
	    ?>
	    <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>		        	
	    	<?php get_template_part( 'content', 'post' );  ?>

		<?php endwhile; endif; ?>		    	    
	</article>
</div>


<div class="grid_12 omega clearfix">
	<div class="grid_6">
		<article>
		<h5>Recent Post</h5>
	    <?php 
		    wp_reset_postdata();
	        $args = array(
	            'post_type' => 'post',
	            'cat' => '-4',
	            'posts_per_page' => 1
	        );
	        $the_query = new WP_Query( $args );		    
	    ?>		    
	    <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>		        	
	    	<?php get_template_part( 'content', 'post' );  ?>

		<?php endwhile; endif; ?>		    	    

		</article>
	</div>
	<div class="grid_6 omega">
		<h5>Recent Project</h5>

		<?php 
			wp_reset_postdata();
		    $args = array(
		        'post_type' => 'work',
		        'posts_per_page' => 1
		    );
		    $the_query = new WP_Query( $args );
		?>
		<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

		<div class="spotlight" style="background-color: <?php the_field('background_color'); ?>">
		<?php get_template_part( 'loop', 'portfolio' ); ?>
		</div>
		
		<?php endwhile; endif; ?>			

		</div>
	</div>		



<?php get_footer(); ?>