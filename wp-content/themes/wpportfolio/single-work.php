<?php get_header(); ?>

<div class="grid_12 omega clearfix">

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<h3><?php the_title() ;?></h3>	
		<div class="intro">
			<p><?php the_field( 'description' ); ?></p>
		</div>
		
		<p>
			<a class="btn blue" href="<?php the_field( 'url' ); ?>" style="background-color: <?php the_field( 'button_color' ); ?>">
				View Project &rarr;
			</a>
		</p>

		<hr>

		<div class="project-images">

			<?php the_field( 'project_images' ); ?>

		</div>


		<?php 
		
		$testimonials = get_field( 'related_testimonials' ); 

		foreach ($testimonials as $testimonial):
			$name = get_field( 'name', $testimonial->ID );		
			$testimonial = get_field( 'testimonial', $testimonial->ID );			
		?>

		<div class="testimonial push_2 grid_10 clearfix">
			<blockquote>&ldquo; <?php echo $testimonial; ?> &rdquo;</blockquote>
			<cite>&mdash; <?php echo $name; ?></cite>
		</div>

		<?php endforeach; ?>

	<?php endwhile; else: ?>
		
		<p>There are no posts or pages here</p>

	<?php endif; ?>

</div>

<?php get_footer(); ?>